<?php

namespace App\Http\Controllers;

use App\Mail\EmergencyCallReceived;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function mail()
    {
        $mail = '1';
        Mail::to('andyxmichi@gmail.com')->send(new EmergencyCallReceived(['mail' => ['nombre' => 'Andrea Aguirre']]));
        return 'true';
    }
}
